﻿using System;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class SelectOptions : Form
    {
        public string Bearer = "";

        public SelectOptions(string Token)
        {
            Bearer = Token;
            InitializeComponent();
            BearerText.Text = "Bearer: " + Bearer;
        }

        private void DefaultAvatar_Click(object sender, EventArgs e)
        {
            SetAvatar avaForm = new SetAvatar(Bearer);
            avaForm.Show();
            this.Hide();
        }

        private void SelectOptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.loginFormChrome.LogoutOfPSNAndExit();
        }

        private void RemoveName_Click(object sender, EventArgs e)
        {
            RemoveName remForm = new RemoveName(Bearer);
            remForm.Show();
            this.Hide();
        }

        private void EndSession_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.loginFormChrome.LogoutOfPSN();
        }

        private void customColor_Click(object sender, EventArgs e)
        {
            SetColor colForm = new SetColor(Bearer);
            colForm.Show();
            this.Hide();
        }

        private void removeAddress_Click(object sender, EventArgs e)
        {
            RemoveAddress addrForm = new RemoveAddress(Bearer);
            addrForm.Show();
            this.Hide();
        }

        private void getSecrets_Click(object sender, EventArgs e)
        {
            this.Hide();
            AccountSecrets accountSecrets = new AccountSecrets(Bearer);
            accountSecrets.Show();
        }
    }
}
