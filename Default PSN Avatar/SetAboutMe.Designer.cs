﻿namespace Default_PSN_Avatar
{
    partial class SetAboutMe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetAboutMe));
            this.label1 = new System.Windows.Forms.Label();
            this.BearerText = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.aboutMe = new System.Windows.Forms.RichTextBox();
            this.setAbout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Created By: SilicaAndPina";
            // 
            // BearerText
            // 
            this.BearerText.AutoSize = true;
            this.BearerText.Location = new System.Drawing.Point(12, 9);
            this.BearerText.Name = "BearerText";
            this.BearerText.Size = new System.Drawing.Size(44, 13);
            this.BearerText.TabIndex = 3;
            this.BearerText.Text = "Bearer: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(121, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "All characters: Unicode and Ascii Supported ;)";
            // 
            // aboutMe
            // 
            this.aboutMe.Location = new System.Drawing.Point(12, 103);
            this.aboutMe.Name = "aboutMe";
            this.aboutMe.Size = new System.Drawing.Size(346, 361);
            this.aboutMe.TabIndex = 11;
            this.aboutMe.Text = "";
            // 
            // setAbout
            // 
            this.setAbout.Location = new System.Drawing.Point(12, 470);
            this.setAbout.Name = "setAbout";
            this.setAbout.Size = new System.Drawing.Size(346, 40);
            this.setAbout.TabIndex = 12;
            this.setAbout.Text = "Set \"About Me\"";
            this.setAbout.UseVisualStyleBackColor = true;
            this.setAbout.Click += new System.EventHandler(this.setAbout_Click);
            // 
            // SetAboutMe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 522);
            this.Controls.Add(this.setAbout);
            this.Controls.Add(this.aboutMe);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BearerText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetAboutMe";
            this.Text = "Set About Me";
            this.Load += new System.EventHandler(this.SetAboutMe_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label BearerText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox aboutMe;
        private System.Windows.Forms.Button setAbout;
    }
}