﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace Default_PSN_Avatar
{


    public partial class RemoveName : Form
    {
        public string Bearer = "";

        public RemoveName(string Token)
        {
            Bearer = Token;
            InitializeComponent();
        }

        private void RemoveName_Load(object sender, EventArgs e)
        {
            BearerText.Text = "Bearer: " + Bearer;
        }

        private void RemoveName_FormClosing(object sender, FormClosingEventArgs e)
        {
            SelectOptions selOption = new SelectOptions(Bearer);
            selOption.Show();
            this.Hide();
        }



        private void DelName_Click(object sender, EventArgs e)
        {
            try
            {
                DelName.Enabled = false;

                var GetCurrent = new WebClient();
 
                GetCurrent.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
                GetCurrent.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
                GetCurrent.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
                GetCurrent.Headers.Set("Authorization", "Bearer " + Bearer);

                string json = GetCurrent.DownloadString("https://accounts.api.playstation.com/api/v1/accounts/me/communication");
                GetCurrent.Dispose();

                int Index = json.IndexOf("realName");
                string OldSettings = json.Substring(0, Index);

                string MySettingsFirst = "realName\":{\"name\":{\"first\":\""+firstName.Text+"\"";
                string MySettingsMiddle = "";
                if(lastName.Text != "")
                {
                    MySettingsMiddle = ",\"middle\":\"" + middleName.Text + "\"";
                }
                string MySettingsLast = ",\"last\":\""+lastName.Text+"\"}}}";

                string FullJson = OldSettings + MySettingsFirst + MySettingsMiddle + MySettingsLast;

                var RemoveRequest = new ExpectContinueAware();

                RemoveRequest.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
                RemoveRequest.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
                RemoveRequest.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
                RemoveRequest.Headers.Set("Authorization", "Bearer " + Bearer);
                RemoveRequest.Headers.Set("Content-Type", "application/json; charset=UTF-8");


                string Response = RemoveRequest.UploadString("https://accounts.api.playstation.com/api/v1/accounts/me/communication", "PUT", FullJson);
                if (Response == "")
                {
                    MessageBox.Show("Real Name Changed Successfully.", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(Response, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                GetCurrent.Dispose();
                DelName.Enabled = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message+"\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                DelName.Enabled = true;
            }
        }
           

    }

}

