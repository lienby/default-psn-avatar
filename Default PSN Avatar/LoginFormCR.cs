﻿using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class LoginFormCR : Form
    {
        public ChromiumWebBrowser chromeBrowser;
        bool IsLoggingOut = false;
        bool IsClosing = false;
        bool GotSso = false;
        public static string userAgentString;
        public static string ssoCookie = null;
        public static string bearer;

        String DefaultPage = @"https://id.sonyentertainmentnetwork.com/signin/?ui=pr&response_type=token&scope=openid%3Auser_id%20openid%3Aonline_id%20openid%3Actry_code%20openid%3Alang%20user%3Aaccount.communication.get%20kamaji%3Aget_account_hash%20oauth%3Amanage_user_auth_sessions%20openid%3Aacct_uuid%20user%3Aaccount.authentication.mode.get%20user%3Aaccount.phone.masked.get%20user%3Aaccount.notification.create%20openid%3Acontent_ctrl%20user%3Aaccount.subaccounts.get%20kamaji%3Aget_internal_entitlements%20user%3AverifiedAccount.get%20kamaji%3Aaccount_link_user_link_account%20kamaji%3Aactivity_feed_set_feed_privacy%20user%3Aaccount.identityMapper%20user%3Aaccount.email.create%20user%3Aaccount.emailVerification.get%20user%3Aaccount.tosua.update%20user:account.communication.update.gated%20user:account.communication.update%20user:account.profile.get%20user:account.profile.update%20user:account.address.create.gated%20user:account.address.update.gated%20user:account.address.get.gated%20user%3Aaccount.profile.get%20user%3Aaccount.profile.update&redirect_uri=https%3A%2F%2Fid.sonyentertainmentnetwork.com%2Fid%2Fmanagement%2F%3Fentry%3Dpsn_profile%26pr_referer%3Dcam&client_id=ce381e15-9cdd-4cf9-8384-0cf63db17f6a&state=8e12a56393f07935a356f7554f9bb29&error=login_required&error_code=4165&error_description=User+is+not+authenticated&no_captcha=false#/signin?entry=%2Fsignin";
        public void LogoutOfPSN()
        {
            IsLoggingOut = true;
            IsUrlGoneTo = false;
            GotSso = false;
            chromeBrowser.EvaluateScriptAsync("document.getElementsByClassName(\"row-unshrink signout-button\")[0].children[0].click()");
        }
        public string GetLatestChromiumUserAgent()
        {
            try
            {
                using (ExpectContinueAware wc = new ExpectContinueAware())
                {
                    // I am the smarted programmer to ever live!.

                    // Download latest packages list from googles APT repo
                    string chromePackages = Encoding.UTF8.GetString(wc.DownloadData("https://dl.google.com/linux/chrome/deb/dists/stable/main/binary-amd64/Packages")).Replace("\r", "");
                    // Parse some packages list to extract version number
                    string version = "";
                    foreach(string pkItem in chromePackages.Split('\n'))
                    {
                        if(pkItem.StartsWith("Version: "))
                        {
                            version = pkItem.Substring("Version: ".Length);
                            int foundDash = version.IndexOf('-');
                            if(foundDash != -1)
                            {
                                version = version.Substring(0, foundDash);
                            }
                        }
                    }
                    version = version.Replace("\n", "");
                    // construct user-agent! 
                    return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/" + version + " Safari/537.36";

                    /*wc.Headers.Set("Content-Type", "application/x-www-form-urlencoded");
                    string chromeUserAgent = Encoding.UTF8.GetString(wc.UploadData("https://user-agents.net/download", "POST", Encoding.UTF8.GetBytes("browser=chrome&browser_bits=64&browser_maker=google-inc&browser_type=browser&platform=win10&limit=1&download=txt")));
                    return HttpUtility.UrlDecode(chromeUserAgent);*/
                }
            }
            catch (WebException)
            {
                Random random = new Random(Guid.NewGuid().GetHashCode());
                string chromeVersion = random.Next(100, 105).ToString() + "." + random.Next(0, 9).ToString() + "." + random.Next(1000, 9999).ToString() + "." + random.Next(100, 999).ToString();
                string browser = "Chrome";
                return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " + browser + "/" + chromeVersion + " Safari/537.36"; // Fingerprinting BTFO'd
            }

        }

        public void LogoutOfPSNAndExit()
        {
            IsClosing = true;
            LogoutOfPSN();
        }

        public void InitializeChromium(string ua)
        {
            IsUrlGoneTo = false;
            if (chromeBrowser != null)
            {
                chromeBrowser.GetDevToolsClient().Emulation.SetUserAgentOverrideAsync(ua);
                chromeBrowser.Load(DefaultPage);
                return;
            }

            CefSettings settings = new CefSettings();
            settings.LogSeverity = LogSeverity.Disable;
            settings.UserAgent = ua;
            settings.PersistSessionCookies = false;
            settings.PersistUserPreferences = false;
            Cef.Initialize(settings);
            Cef.GetGlobalCookieManager().DeleteCookies("", "");
            chromeBrowser = new ChromiumWebBrowser(DefaultPage);
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
            chromeBrowser.AddressChanged += ChromeBrowser_AddressChanged;
            chromeBrowser.LoadingStateChanged += ChromeBrowser_LoadingStateChanged;
        }

        private void ChromeBrowser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (e.IsLoading)
                return;

            if (chromeBrowser.Address.Contains("ssocookie"))
            {
                chromeBrowser.EvaluateScriptAsync("document.documentElement.innerText.toString()").ContinueWith(x =>
                {
                    GotSso = true;
                    chromeBrowser.Load(@"https://id.sonyentertainmentnetwork.com/id/management/#/p/psn_profile/list?entry=psn_profile");

                    string npsso = x.Result.Result.ToString();
                    dynamic response = JsonConvert.DeserializeObject(npsso);
                    if (response.npsso == null)
                    {
                        MessageBox.Show("Failed to get npsso :(", "NpSSO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        ssoCookie = response.npsso;
                    }
                });
            }
        }

        public LoginFormCR()
        {
            InitializeComponent();
        }
        public bool IsUrlGoneTo;

        private void ChromeBrowser_AddressChanged(object sender, AddressChangedEventArgs e)
        {

            if (IsLoggingOut)
            {
                if (chromeBrowser.Address.StartsWith("https://id.sonyentertainmentnetwork.com/signin"))
                {
                    IsLoggingOut = false;
                    if (IsClosing)
                    {
                        Invoke(new Action(() =>
                        {
                            chromeBrowser.Dispose();

                            try
                            {
                                Application.Exit();
                            }
                            catch (Exception) { };

                            Process.GetCurrentProcess().Kill(); //kys
                        }));

                    }
                    Invoke(new Action(() =>
                    {
                        Show();
                    }));
                }
            }
            


            if (chromeBrowser.Address.Contains("access_token"))
            {

                if (!GotSso)
                {
                    int i = chromeBrowser.Address.IndexOf("access_token=") + 13;
                    string Bearer = chromeBrowser.Address.Substring(i);
                    i = Bearer.IndexOf("&");
                    bearer = Bearer.Substring(0, i);

                    chromeBrowser.Load("https://auth.api.sonyentertainmentnetwork.com/2.0/ssocookie");

                    SelectOptions selOption = new SelectOptions(bearer);
                    selOption.Show();

                    Invoke(new Action(() =>
                    {
                        Hide();
                    }));

                }
            }
        }

        private void LoginFormCR_Load(object sender, EventArgs e)
        {

            Random random = new Random(Guid.NewGuid().GetHashCode());
            this.Size = new Size(574 + random.Next(-10, 100), 685 + random.Next(-10, 100));
            userAgentString = GetLatestChromiumUserAgent();
            userAgent.Text = userAgentString;
            InitializeChromium(userAgent.Text);
        }

        private void userAgent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                InitializeChromium(userAgent.Text);
            }
        }
    }
}
