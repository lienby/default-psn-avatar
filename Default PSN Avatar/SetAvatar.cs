﻿
using System;
using System.Net;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class SetAvatar : Form
    {
        public string Bearer = "";

        public SetAvatar(string Token)
        {
            Bearer = Token;
            InitializeComponent();
        }

        private void SetAvatar_Load(object sender, System.EventArgs e)
        {
            BearerText.Text = "Bearer: " + Bearer;
        }

        private void SetAvi_Click(object sender, EventArgs e)
        {
            try
            {
                SetAvi.Enabled = false;
                var ChangeRequest = new WebClient();
                ChangeRequest.Headers.Set("Content-Type", "application/json; charset=UTF-8-Type");
                ChangeRequest.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
                ChangeRequest.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
                ChangeRequest.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
                ChangeRequest.Headers.Set("Authorization", "Bearer " + Bearer);

                string Response = ChangeRequest.UploadString("https://us-prof.np.community.playstation.net/userProfile/v1/users/me/avatar", "PUT", "{\"avatarId\":0}");
                if (Response == "")
                {
                    MessageBox.Show("Avatar Changed Successfully!", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(Response, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                SetAvi.Enabled = true;
            }
            catch(Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                SetAvi.Enabled = false;
            }

        }

        private void SetAvatar_FormClosing(object sender, FormClosingEventArgs e)
        {
            SelectOptions selOption = new SelectOptions(Bearer);
            selOption.Show();
            this.Hide();
        }
    }
}
